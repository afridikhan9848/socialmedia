import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  msg:string = "";
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  testHello(){
    this.dataService.testHello().subscribe((result:any)=>{
      this.msg = "OK";
      console.log(result);
    })
  }
}
