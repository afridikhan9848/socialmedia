import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RefreshTokenRequest } from './interface/RefreshTokenRequest'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl:String = "http://192.168.43.172:9090/";

  constructor(private httpClient:HttpClient) { }


  login(credentials:any){
    return this.httpClient.post(this.baseUrl + "api/auth/login",credentials);
  }

  refreshToken(){
    let refreshTokenRequest:RefreshTokenRequest = new RefreshTokenRequest();
    refreshTokenRequest.refreshToken= localStorage.refreshToken;
    refreshTokenRequest.username = localStorage.username;
    return this.httpClient.post(this.baseUrl+"api/auth/refresh/token", refreshTokenRequest);
  }

}
