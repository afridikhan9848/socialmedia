import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AuthenticationResponse } from '../interface/AuthenticationResponse'
import { error } from '@angular/compiler/src/util';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService) { }

  ngOnInit(): void {
  }

  login(credentials:any){
    console.log(credentials);
    this.authService.login(credentials).subscribe((result:AuthenticationResponse)=>{
      localStorage.setItem('authenticationToken', result.authenticationToken);
      localStorage.setItem('expiresAt', result.expiresAt);
      localStorage.setItem('refreshToken', result.refreshToken);
      localStorage.setItem('username', result.username);
      console.log(result);
    })
  }

}
