import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/shared/home/home/home.component';
import { LoginComponent } from './components/auth/login/login.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { ForgotPassComponent } from './components/auth/forgot-pass/forgot-pass.component';
import { DetailedPostComponent } from './components/shared/post/detailed-post/detailed-post.component';
import { AddNewPostComponent } from './components/shared/home/add-new-post/add-new-post.component';
import { AddNewCommentComponent } from './components/shared/post/add-new-comment/add-new-comment.component';
import { NavComponent } from './components/shared/nav/nav/nav.component';
import { CommentsComponent } from './components/shared/post/comments/comments.component';
import { NotificationComponent } from './components/shared/nav/notification/notification.component';
import { FriendRequestComponent } from './components/shared/nav/friend-request/friend-request.component';
import { SharedPostComponent } from './components/shared/post/shared-post/shared-post.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { EditProfileComponent } from './components/user/edit-profile/edit-profile.component';
import { FriendsListComponent } from './components/user/friends-list/friends-list.component';
import { FriendsSuggestionComponent } from './components/user/friends-suggestion/friends-suggestion.component';
import { UnkownFriendComponent } from './components/user/friendProfile/unkown-friend/unkown-friend.component';
import { KownFriendComponent } from './components/user/friendProfile/kown-friend/kown-friend.component';
import { FirendProfileComponent } from './components/user/friendProfile/firend-profile/firend-profile.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {path:'' , component: HomeComponent, canActivate: [AuthGuard]},
  {path:'home' , component: HomeComponent, canActivate: [AuthGuard]},
  {path:'login' , component: LoginComponent},
  {path:'signup' , component: SignupComponent},
  {path:'forgotpass' , component: ForgotPassComponent},
  {path:'detailedpost/:postId' , component: DetailedPostComponent, canActivate: [AuthGuard]},
  {path:'addnewpost' , component: AddNewPostComponent, canActivate: [AuthGuard]},
  {path:'addnewcomment' , component: AddNewCommentComponent, canActivate: [AuthGuard]},
  {path:'comments' , component: Comment, canActivate: [AuthGuard]},
  {path:'profile' , component: ProfileComponent, canActivate: [AuthGuard]},
  {path:'editprofile' , component: EditProfileComponent, canActivate: [AuthGuard]},
  {path:'friendslist' , component: FriendsListComponent, canActivate: [AuthGuard]},
  {path:'friendprofile/:username' , component: FirendProfileComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const AppRountingComponent = [
          HomeComponent,
          LoginComponent,
          SignupComponent,
          ForgotPassComponent,
          DetailedPostComponent,
          AddNewPostComponent,
          AddNewCommentComponent,
          NavComponent,
          CommentsComponent,
          NotificationComponent,
          FriendRequestComponent,
          SharedPostComponent,
          ProfileComponent,
          EditProfileComponent,
          FriendsListComponent,
          FriendsSuggestionComponent,
          UnkownFriendComponent,
          KownFriendComponent,
          FirendProfileComponent
]
