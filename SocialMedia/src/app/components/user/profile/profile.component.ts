import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/interface/post';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  posts:Post[] = [];
  constructor(private postService:PostService) { }

  ngOnInit(): void {
      this.postService.getPostsByUsername(localStorage.username).subscribe((result:Post[])=>{
        this.posts =result.reverse();
      })
  }

}
