import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KownFriendComponent } from './kown-friend.component';

describe('KownFriendComponent', () => {
  let component: KownFriendComponent;
  let fixture: ComponentFixture<KownFriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KownFriendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KownFriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
