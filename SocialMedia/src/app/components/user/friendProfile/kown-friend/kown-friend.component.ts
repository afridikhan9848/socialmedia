import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/interface/post';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-kown-friend',
  templateUrl: './kown-friend.component.html',
  styleUrls: ['./kown-friend.component.css'],
})
export class KownFriendComponent implements OnInit {
  @Input() username:string;
  posts: Post[] = [];
  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.postService.getPostsByUsername(this.username).subscribe((result: Post[]) => {
        this.posts = result;
        console.log(this.posts);
      });
  }
}
