import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { HttpEventType } from '@angular/common/http';
import { Post } from 'src/app/interface/post';

@Component({
  selector: 'app-add-new-post',
  templateUrl: './add-new-post.component.html',
  styleUrls: ['./add-new-post.component.css'],
})
export class AddNewPostComponent implements OnInit {
  fileData: File;
  previewUrl: any;
  uploadedFilePath: string;
  fileUploadProgress: String;
  uploadingStatus: boolean = false;
  uploadingSuccess: boolean = false;

  constructor(private postService: PostService) {}

  ngOnInit(): void {}

  fileProgress(fileInput: any) {
    console.log(fileInput);
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = () => {
      this.previewUrl = reader.result;
    };
  }

  onSubmit(caption:string) {
    const formData = new FormData();
    formData.append('image', this.fileData);
    this.fileUploadProgress = '0%';
    this.uploadingStatus = true;

    this.postService.imageUpload(formData).subscribe((events: any) => {
      if (events.type === HttpEventType.UploadProgress) {
        this.fileUploadProgress = Math.round((events.loaded / events.total) * 100) + '%';
      }
      if (events.type === HttpEventType.Response) {
        console.log('upload completed');
        console.log(events.body.imageUrl);
        this.fileUploadProgress = '';

        let newPost: Post = new Post();
        newPost.imageUrl = events.body.imageUrl;
        newPost.username = localStorage.username;
        newPost.caption = caption;

        this.postService.addNewPost(newPost).subscribe((result: any) => {
          console.log(result);
          console.log('All Done....');
          this.uploadingStatus = false;
          this.uploadingSuccess = true;
        });

      }
    });
  }
}
