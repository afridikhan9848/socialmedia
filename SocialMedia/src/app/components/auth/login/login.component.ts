import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { AuthenticationResponse } from '../../../interface/auth'
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginErrorMsg:boolean = false;
  constructor(private authService:AuthService,
              private router:Router) { }

  ngOnInit(): void {
  }

  login(credentials:any){
    //console.log(credentials);
    this.authService.login(credentials).subscribe((result:AuthenticationResponse)=>{
      localStorage.setItem('authenticationToken', result.authenticationToken);
      localStorage.setItem('expiresAt', result.expiresAt);
      localStorage.setItem('refreshToken', result.refreshToken);
      localStorage.setItem('username', result.username);
      this.authService.subscriptionForLogin.next(true);
     // console.log(result);
      this.router.navigate(['/home'])
    },(error:any)=>{
      this.loginErrorMsg = true;
    })
  }

}
