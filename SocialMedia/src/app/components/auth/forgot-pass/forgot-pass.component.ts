import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { RefreshTokenRequest, OtpVerificationRequest, ChangePasswordRequest } from '../../../interface/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.css']
})
export class ForgotPassComponent implements OnInit {
  usernameError:boolean = false;
  OtpError:boolean = false;
  forgotPassDiv:boolean= true;
  enterOtpDiv:boolean = false;
  changePasswordDiv:boolean = false;
  passMatchingError:boolean = false;
  successMsgDiv:boolean = false;

  constructor(private authService:AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  forgotPassword(data:any){
    this.authService.forgotPassword(data.username).subscribe((result:OtpVerificationRequest)=>{
      if (result !== null) {
        this.enterOtpDiv = true;
        this.forgotPassDiv = false;
        localStorage.setItem("otpToken", result.otpToken);
        localStorage.setItem("username", result.username);
      }
    },(error:any)=>{
        this.usernameError = true;
    });
  }

  OtpVerification(otpNumber:number){
    let otpVerificationRequest:OtpVerificationRequest = new OtpVerificationRequest();
    otpVerificationRequest.otpNumber = otpNumber;
    otpVerificationRequest.otpToken = localStorage.otpToken;
    otpVerificationRequest.username = localStorage.username;
    this.authService.forgotPasswordVerification(otpVerificationRequest).subscribe((result:boolean)=>{
        this.changePasswordDiv = true;
        this.enterOtpDiv = false;
    },(error:any)=>{
      this.OtpError = true;
    })
  }

  changePassword(changePasswordRequest:ChangePasswordRequest){
    if(changePasswordRequest.password === changePasswordRequest.confirmPassword){
      changePasswordRequest.username = localStorage.username;
      changePasswordRequest.otpToken = localStorage.otpToken;
      this.authService.changePassword(changePasswordRequest).subscribe((result:boolean)=>{
        console.log(result);
        this.passMatchingError = true;
        this.changePasswordDiv = false;
        this.successMsgDiv = true
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 2000);
      })
    } else{
      this.passMatchingError = true;
    }
  }


}
