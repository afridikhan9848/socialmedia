export interface AuthenticationResponse {
  authenticationToken: string;
  refreshToken: string;
  expiresAt: string;
  username: string;
}

export class RefreshTokenRequest {
  refreshToken: String;
  username: String;
  constructor() {}
}

export class OtpVerificationRequest {
  otpNumber: number;
  username: string;
  otpToken: string;
  constructor() {}
}

export class ChangePasswordRequest {
  password: string;
  confirmPassword: string;
  otpToken: string;
  username: string;
  constructor() {}
}

export interface RegisterUser {
  email: string;
  gender: string;
  password: string;
  username: string;
}
