import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post, CommentRequest , Commet } from 'src/app/interface/post';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  nodeJsUrl:string = environment.NODEJS_URL;
  hostUrl:string = environment.HOST_URL;
  refreshSubject = new Subject<void>();

  constructor( public httpClient:HttpClient) {}

  imageUpload(image:any){
    return this.httpClient.post( this.nodeJsUrl +'image/upload',image,
                                  {reportProgress:  true, observe: 'events'})
  }

  addNewPost(newPost:Post){
    return this.httpClient.post(this.hostUrl + "api/post/new/post", newPost);
  }

  addNewComment(newComment:CommentRequest){
    return this.httpClient.post(this.hostUrl + "api/post/new/comment", newComment).pipe(
      tap(()=>{
        this.refreshSubject.next();
      })
    )
  }

  getPostByPostId(postId:number){
    return this.httpClient.get(this.hostUrl + "api/post/" + postId);
  }

  getAllFriendsPosts(){
    return this.httpClient.get(this.hostUrl + "api/post/all/posts");
  }

  getPostCommentsByPostCommentId(postCommentId:string){
    return this.httpClient.get(this.hostUrl + "api/post/comment/" + postCommentId);
  }

  getPostsByUsername(username:string){
    return this.httpClient.get(this.hostUrl+"api/post/all/"+username);
  }
}
