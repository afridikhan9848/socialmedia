Auth -
	Components
		Login **
		Signup **
		forgotPass *o
    Services
		AuthService *

User -
	Componenets
		Profile **
		EditProfile *o ////need to add form
		FriendsList **
		FriendsSuggestion **
		FriendProfile *
			UnkownFriend (Lock) *
			KnownFriend (Open) *
	Services -
		UserService *
			

Shared -
	Nav -
		Componenets -
			nav **
			Notification **
			FriendRequest **
		Services -
			NavService *
	Home -
		Componenets -
			AddNewPost **
		Services -
			HomeService *
	Post -
		Componenets -
			SharedPost **
			DetailedPost **
			AddNewComment **
			Comments **
		Services -
			PostService *

Guard - CanActivate
HttpInterceptor